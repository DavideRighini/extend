#include <systemc.h>
#include "extend.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

static const unsigned TEST_SIZE = 6;
static const unsigned DATA_WIDTH = 16;
sc_fifo<int> observer_extend;
int values1[TEST_SIZE]; //vettore usato per tutti i test
int values2[TEST_SIZE]; //vettore usato per tutti i test

SC_MODULE(TestBench) 
{
 	public:
	
	sc_signal<sc_lv<7> > extend_din;
	sc_signal<sc_lv<DATA_WIDTH> > extend_dout;
	
	Extend extend1 ; //EXTEND SIGN

  SC_CTOR(TestBench) : extend1("extend1")
  {
 		init_values();

		SC_THREAD(init_values_extend_test);
		extend1.din(this->extend_din);
		extend1.dout(this->extend_dout);		
		SC_THREAD(observer_thread_extend);
		sensitive << extend1.dout;
	}

	void init_values() {
				values1[0] = 5;
				values1[1] = 4;
				values1[2] = 3;
				values1[3] = 2;
				values1[4] = 1;
				values1[5] = 0;
				
				values2[0] = 1;
				values2[1] = 3;
				values2[2] = 5;
				values2[3] = 7;
				values2[4] = 9;
				values2[5] = 0;
						  		  
	}
	
	void init_values_extend_test() {
		for (unsigned i=0;i<TEST_SIZE;i++) {
		  extend_din.write(values1[i]); 
		  wait(10,SC_NS);
		}
		wait(50,SC_NS);
		sc_stop();
	}

	void observer_thread_extend() {
		while(true) {
				wait();
				int value = extend1.dout->read().to_int();
				cout << "observer_thread: at " << sc_time_stamp() << " extend out: " << extend1.dout->read() << endl;
				if (observer_extend.num_free()>0 ) observer_extend.write(value);
		} 
	}

	int check_extend() {
		int test=0;
		for (unsigned i=0;i<TEST_SIZE;i++) {
				test=observer_extend.read(); //porta fifo
		    if (test != values1[i])
		        return 1;
		}
		return 0;
	}

};

// ---------------------------------------------------------------------------------------

int sc_main(int argc, char* argv[]) {
	
	TestBench testbench("testbench");
	sc_start();

  return testbench.check_extend();
}
