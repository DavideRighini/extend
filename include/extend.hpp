#ifndef EXTEND_HPP
#define EXTEND_HPP

SC_MODULE(Extend) {
	static const unsigned DATA_W_IN = 7;
	static const unsigned DATA_W_OUT = 16;
		
	sc_in<sc_lv<DATA_W_IN> > din;
	sc_out<sc_lv<DATA_W_OUT> > dout;


	SC_CTOR(Extend) {
		SC_THREAD(update_ports);
			sensitive << din;
	}
	
	private:
	void update_ports();
};

#endif
